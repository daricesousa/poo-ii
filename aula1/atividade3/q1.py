#triangulo

a, b, c = input("Digite 3 lados do triangulo: ").split()
a = int(a)
b = int(b)
c = int(c)

if a > b+c or b> a+c or c> a+b:
    print("Não formam um triangulo")
elif a == b and b == c:
    print("equilatero")
elif a!= b and b!= c:
    print("escaleno")
else:
    print("isosceles")