#combustivel

litros = float(input("Litros: "))
combustivel = input("A-alcool, G-gasolina: ")
preco = 0

if combustivel == 'G':
    preco = litros * 4.53
    if(litros<=20):
        preco -= preco*4/100
    else:
        preco -= preco*6/100
    
elif combustivel == 'A':
    preco = litros * 3.45
    if(litros<=20):
        preco -= preco*3/100
    else:
        preco -= preco*5/100

print(preco)