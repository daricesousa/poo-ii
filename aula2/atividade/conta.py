from historico import Historico

class Conta:
    def __init__(self, numero, cliente, saldo, limite=500):
        self.numero = numero
        self.cliente = cliente
        self.saldo = saldo
        self.limite = limite
        self.historico = Historico()

    def deposita(self, valor):
        self.saldo += valor
        self.historico.transacoes.append("deposito de R${}".format(valor))

    def saca(self, valor):
        if valor>self.saldo:
            return False
        else:
            self.saldo -= valor
            self.historico.transacoes.append("saque de R${}".format(valor))
            return True
    
    def extrato(self):
        print("conta: {}\nsaldo: {}".format(self.numero, self.saldo))
        self.historico.transacoes.append("extrato efetuado. Saldo de de R${}".format(self.saldo))

    def transfere(self, destino, valor):
        valorDisponivel = self.saca(valor)
        if valorDisponivel:
            destino.deposita(valor)
            self.historico.transacoes.append("transferencia de R$ {} realizada para {}".format(valor, destino.numero))
            return True
        else:
            return False