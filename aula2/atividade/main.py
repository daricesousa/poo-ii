from cliente import Cliente
from conta import Conta

cliente1 = Cliente('Darice', 'Sousa', '666')
cliente2 = Cliente('Weliton', 'Sousa', '777')

c1 = Conta('123', cliente1, 100, 500)
c2 = Conta('456', cliente2, 100)

c1.deposita(20)

c1.saca(10)

print(c1.transfere(c2, 50))

print(c1.transfere(c2, 200))

c1.extrato()

c1.historico.imprime()