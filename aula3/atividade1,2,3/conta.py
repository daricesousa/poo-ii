from historico import Historico

class Conta:

    __slots__ = ['_numero', '_cliente', '_saldo', '_limite', '_historico']

    _totalContas = 0

    def __init__(self, numero, cliente, saldo, limite=500):
        self._numero = numero
        self._cliente = cliente
        self._saldo = saldo
        self._limite = limite
        self._historico = Historico()
        Conta._totalContas += 1

    @staticmethod
    def totalContas():
        return Conta._totalContas

    @property
    def numero(self):
        return self._numero

    @numero.setter
    def numero(self, numero):
        self._numero = numero

    @property
    def cliente(self):
        return self._cliente

    @cliente.setter
    def cliente(self, cliente):
        self._cliente = cliente

    @property
    def saldo(self):
        return self._saldo

    @property
    def limite(self):
        return self._limite

    @limite.setter
    def limite(self, limite):
        self._limite = limite

    @property
    def historico(self):
        return self._historico

    @historico.setter
    def historico(self, historico):
        self._historico = historico


    def deposita(self, valor):
        self._saldo += valor
        self.historico.transacoes.append("deposito de R${}".format(valor))

    def saca(self, valor):
        if valor>self.saldo:
            return False
        else:
            self._saldo -= valor
            self.historico.transacoes.append("saque de R${}".format(valor))
            return True
    
    def extrato(self):
        print("conta: {}\nsaldo: {}".format(self.numero, self.saldo))
        self.historico.transacoes.append("extrato efetuado. Saldo de de R${}".format(self.saldo))

    def transfere(self, destino, valor):
        valorDisponivel = self.saca(valor)
        if valorDisponivel:
            destino.deposita(valor)
            self.historico.transacoes.append("transferencia de R$ {} realizada para {}".format(valor, destino.numero))
            return True
        else:
            return False

    