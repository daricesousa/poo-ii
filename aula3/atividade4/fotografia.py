from skimage import io
from datetime import datetime
from matplotlib import pyplot as plt

class Fotografia:

    __slots__ = ['_foto', '_fotografo', '_proprietario', '_data']

    _quantidadeFotos = 0

    def __init__(self, foto, fotografo, proprietario):
        self._foto = io.imread(foto)
        self._fotografo = fotografo
        self._proprietario = proprietario
        self._data = datetime.now()
        Fotografia._quantidadeFotos += 1


    def mostrar(self):
        plt.imshow(self.foto)
        plt.show()

    def propriedades(self):
        print("tamanho em pixels:{}x{}".format(self.foto.shape[0],self.foto.shape[1]))
        print("fotografo:", self.fotografo.nome)
        print("data", self.data)




    @property
    def foto(self):
        return self._foto

    @foto.setter
    def foto(self, foto):
        self._foto = foto

    @property
    def fotografo(self):
        return self._fotografo

    @fotografo.setter
    def fotografo(self, fotografo):
        self._fotografo = fotografo

    @property
    def proprietario(self):
        return self._proprietario

    @proprietario.setter
    def proprietario(self, proprietario):
        self._proprietario = proprietario

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    @staticmethod
    def quantidadeFotos():
        return Fotografia._quantidadeFotos
